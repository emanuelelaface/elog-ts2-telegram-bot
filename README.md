# Elog Telegram Bot for Test Stand 2

This is the telegram bot that reads the Operations eLog and broadcast it to the ESS subscribers.


## Development

You should use docker for development:

1. Clone the repository

2. Build the docker images

   ```
   $ docker-compose build
   ```

3. Create a `.env` file with the `ELOGTOKEN` and `TELEGRAMTOKEN` variables

   ```
   $ cat .env
   ELOGTOKEN=token1
   TELEGRAMTOKEN=token2
   ```

4. Start the application

   ```
   $ docker-compose up
   ```


You only need to rebuild the docker image if you change the requirements.txt file.
During development, the working directory is mounted as a volume so you don't need to rebuild the image
when changing the code.
